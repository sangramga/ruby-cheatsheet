Authors: Alexandre ZANNI

# Control structures

## Equality

```ruby
irb(main):001:0> 42 == 42.0 # equal values
=> true
irb(main):002:0> 42.eql?(42.0) # equal values and same type
=> false
```

## Subsumption

Be cautious!

```ruby
irb(main):004:0> 42 === (10..100)
=> false
irb(main):005:0> 42 === (50..100)
=> false
irb(main):006:0> (10..100) === 42
=> true
irb(main):007:0> (50..100) === 42
=> false
irb(main):008:0> Integer === 42
=> true
irb(main):009:0> Integer === 'string'
=> false
irb(main):010:0> 42 === Integer
=> false
irb(main):011:0> /at/ === 'Hi mate'
=> true
irb(main):012:0> /at/ === 'Hello'
=> false
irb(main):013:0> 'Hi mate' === /at/
=> false
```

## if/elsif/else statement

```ruby
val = 1337
if val == 42
  puts "That's always true"
elsif val == 1000
  puts "Nearly right"
elsif val == 1300
  puts "Even closer"
else
  puts "Nah!!"
end
# ouput: Nah!
```

## unless statement

Opposite of `if`

```ruby
val = 1337
unless val == 1337
  puts "Your're the elite!"
else
  puts "Master!"
end
# output: Master!
```

## Logical operators

Symbol  | Word
---     | ---
`&&`    | `and`
`||`    | `or`
`!`     | `not`

## case statement

```ruby
a = 10
case a
when 1
  puts 'One'
when 2
  puts 'Two'
when 5
  puts 'Five'
when 10
  puts 'Ten'
when 42, 1337, 31337
  puts 'Master!'
else
  puts 'Nah!'
end
# output: Ten
```

## while loop

```ruby
a = 0
while a <= 5
  puts a
  a += 1
end
```

## until loop

```ruby
a = 0
until a > 5
  puts a
  a += 1
end
```

## Ranges

```ruby
irb(main):043:0> (41..43).to_a
=> [41, 42, 43]
irb(main):044:0> (41...43).to_a
=> [41, 42]
irb(main):045:0> ('m'..'o').to_a
=> ["m", "n", "o"]
irb(main):046:0> ('y'..'b').to_a
=> []
```

## for loop

```ruby
for i in (1..10)
  puts "i: #{i}"
end
```

## break

```ruby
for i in (1..10)
  break if i > 5
  puts "i: #{i}"
end
```

## next

```ruby
for i in (1..10)
  next if i % 2 == 0
  puts "i: #{i}"
end
```

## loop do

Like a `do while` in others languages.

```ruby
a = 0
loop do
  puts a
  a += 1
  break if a > 10
end
```
