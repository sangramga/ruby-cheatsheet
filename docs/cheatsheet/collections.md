Authors: Alexandre ZANNI

# Collections

## Arrays

```ruby
# Basics
irb(main):047:0> peoples = ["Alice", "Bob", "Eve"]
=> ["Alice", "Bob", "Eve"]
irb(main):048:0> peoples[0]
=> "Alice"
irb(main):049:0> peoples[1]
=> "Bob"
irb(main):050:0> peoples[-1]
=> "Eve"
# Adding elements
irb(main):051:0> foo = [42, "Cindy", 0.02, true]
=> [42, "Cindy", 0.02, true]
irb(main):052:0> foo << 'd'
=> [42, "Cindy", 0.02, true, "d"]
irb(main):053:0> foo.push(1337)
=> [42, "Cindy", 0.02, true, "d", 1337]
irb(main):055:0> foo.insert(2, 0.05)
=> [42, "Cindy", 0.05, 0.02, true, "d", 1337]
# Removing elements
irb(main):056:0> foo.pop
=> 1337
irb(main):057:0> foo
=> [42, "Cindy", 0.05, 0.02, true, "d"]
irb(main):060:0> foo.delete_at(2)
=> 0.05
irb(main):061:0> foo
=> [42, "Cindy", 0.02, true, "d"]
# Display
irb(main):065:0> puts foo
42
Cindy
0.02
true
d
=> nil
irb(main):066:0> print foo
[42, "Cindy", 0.02, true, "d"]=> nil
# Array Ranges
irb(main):068:0> alph = ['a', 'c', 'z', 'e']
=> ["a", "c", "z", "e"]
irb(main):069:0> alph[1..3]
=> ["c", "z", "e"]
```

## Array manipulations

```ruby
# Combining arrays
irb(main):072:0> x = [41, 53]
=> [41, 53]
irb(main):073:0> y = [72, 16, 133]
=> [72, 16, 133]
irb(main):074:0> res = x + y
=> [41, 53, 72, 16, 133]
irb(main):075:0> res = y + x
=> [72, 16, 133, 41, 53]
irb(main):078:0> z = [16, 41, 55]
=> [16, 41, 55]
irb(main):079:0> res - z
=> [72, 133, 53]
# Boolean operations
irb(main):081:0> x
=> [41, 53]
irb(main):082:0> z
=> [16, 41, 55]
irb(main):083:0> x & z
=> [41]
irb(main):085:0> x | z
=> [41, 53, 16, 55]
# Moving elements
irb(main):086:0> z.reverse
=> [55, 41, 16]
irb(main):087:0> z
=> [16, 41, 55]
irb(main):088:0> z.reverse!
=> [55, 41, 16]
irb(main):089:0> z
=> [55, 41, 16]
# Array methods
irb(main):090:0> z.length
=> 3
irb(main):091:0> z.size
=> 3
irb(main):092:0> z.sort
=> [16, 41, 55]
irb(main):093:0> c = [42, 42, 75, 42]
=> [42, 42, 75, 42]
irb(main):094:0> c.uniq
=> [42, 75]
irb(main):095:0> z.freeze # prevent from being modified, there is no unfreeze method
=> [55, 41, 16]
irb(main):096:0> z.pop
RuntimeError: can't modify frozen Array
	from (irb):96:in 'pop'
	from (irb):96
	from /usr/bin/irb:11:in `<main>'
irb(main):102:0> z.frozen?
=> true
irb(main):103:0> z.include?(55)
=> true
irb(main):104:0> z.include?(56)
=> false
irb(main):105:0> z.min
=> 16
irb(main):106:0> z.max
=> 55
```

## Hashes and symbols

A.k.a. *associative arrays*, *maps*, *dictionaries*.

```ruby
# String as key
irb(main):001:0> grades = {"Paul" => 17, "Virginia" => 18, "Seb" => 15}
=> {"Paul"=>17, "Virginia"=>18, "Seb"=>15}
irb(main):002:0> grades["Seb"]
=> 15
# Symbols as key
irb(main):007:0> identity = {:name => "Alex", :age => 22, :sex => "male"}
=> {:name=>"Alex", :age=>22, :sex=>"male"}
irb(main):008:0> identity[:name]
=> "Alex"
# Symbols as key (shorter syntax)
irb(main):009:0> rock = {name: "ruby", color: "red"}
=> {:name=>"ruby", :color=>"red"}
irb(main):010:0> rock[:name]
=> "ruby"
# Some methods
irb(main):011:0> identity.delete(:age)
=> 22
irb(main):013:0> grades.key(18)
=> "Virginia"
irb(main):014:0> grades.invert
=> {17=>"Paul", 18=>"Virginia", 15=>"Seb"}
irb(main):015:0> no = {a: 42, b: 42, c: 42}
=> {:a=>42, :b=>42, :c=>42}
irb(main):016:0> no.invert
=> {42=>:c} # Don't forget the key needs to be unique
irb(main):017:0> rock.keys
=> [:name, :color]
irb(main):018:0> rock.values
=> ["ruby", "red"]
irb(main):019:0> rock.length
=> 2
```

## Nested arrays and hashes

```ruby
# Nested arrays
irb(main):020:0> nest = [[10,11,12], ['a','b','c']]
=> [[10, 11, 12], ["a", "b", "c"]]
irb(main):021:0> nest[1]
=> ["a", "b", "c"]
irb(main):022:0> nest[0][2]
=> 12
# Nested hashes
irb(main):027:0> albums = {:"2010" => {:artist => "The Glitch Mob", :title => "Drink The Sea"}, :"2011" => {:artist => "Cryptex", :title => "Isolated Incidents"}}
=> {:"2010"=>{:artist=>"The Glitch Mob", :title=>"Drink The Sea"}, :"2011"=>{:artist=>"Cryptex", :title=>"Isolated Incidents"}}
irb(main):028:0> albums[:"2011"][:artist]
=> "Cryptex"
```

## Iterators

```ruby
# Range iterator
(1..26).each do |i|
  puts i
end

# Array iterator
letters = ('a'..'z').to_a
alphabet = String.new
letters.each do |c|
  alphabet += c
end

# Hash iterator
num = {one: 1, two: 2, three: 3}
num.each do |k,v|
  puts "#{k}: #{v}"
end
num.each {|k,v| puts "#{k}: #{v}"}
num.each do |k,v| puts "#{k}: #{v}" end

# times iterator
7.times do
  print "7"
end

# Iterator with index
arr = ["red", "blue", "green"]
arr.each_with_index do |color, index| puts "#{index}: #{color}" end
```
