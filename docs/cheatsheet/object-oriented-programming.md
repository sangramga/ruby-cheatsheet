Authors: Alexandre ZANNI

# Object oriented programming

## Classes and objects

```ruby
# Initialization
class Person
  def initialize
    puts "My name ise no one!"
  end
end

irb(main):020:0> Person.new
My name ise no one!
```

## Instance variables

```ruby
# Instance variables
class Color
    @color = "no color"
    def initialize(color)
        @color = color
    end
end

y = Color.new("yellow")
g = Color.new("green")
```

## Instance methods and accessors

```ruby
# Accessors
class Person
    def initialize(name)
        @name = name
    end
    # Getter
    def get_name
        @name
    end
    # Setter
    def set_name(name)
        @name = name
    end
end

irb(main):039:0> p = Person.new("Nicolas")
=> #<Person:0x000056328b740568 @name="Nicolas">
irb(main):040:0> p.get_name
=> "Nicolas"
irb(main):055:0> p.set_name("Phil")
=> "Phil"
irb(main):056:0> p.get_name
=> "Phil"

# Setter helper
class Person
    def set_name=(name)
        @name = name
    end
end

irb(main):063:0> p = Person.new("Alexia")
=> #<Person:0x000056328bbd4cc8 @name="Alexia">
irb(main):064:0> p.set_name = "Alexandra"
=> "Alexandra"
```

## Accessor methods

```ruby
class Person
    attr_reader :name   # getter
    attr_writer :age    # setter (with =)
    attr_accessor :surname, :nick   # setter (with =) + getter
    def initialize(name, age, surname)
        @name = name
        @age = age
        @surname = @nick = surname
    end
end

irb(main):075:0> p = Person.new("André", 22, "Dédé")
=> #<Person:0x000056328bba43e8 @name="André", @age=22, @nick="Dédé", @surname="Dédé">
irb(main):076:0> p.name
=> "André"
irb(main):078:0> p.age = 23
=> 23
irb(main):080:0> p.surname = "Dré"
=> "Dré"
irb(main):081:0> p.nick
=> "Dédé"

# Self keyword
class Person
    attr_reader :age
    attr_accessor :birth
    def initialize(birth)
        @birth = birth
        @age = 2017 - birth.to_i
    end

=begin attr_accessor :birth
    def birth
        @birth
    end

    def birth=(birth)
        @birth = birth
    end
=end

    def modify=(birth)
        # birth = birth? local argument = local argument or birth sugar setter with birth local argument?
        self.birth = birth # self refer to the public instance method
        @age = 2017 - birth.to_i
    end
end

irb(main):032:0> p = Person.new(1985)
=> #<Person:0x000055ab32ed0d90 @birth=1985, @age=32>
irb(main):033:0> p.age
=> 32
irb(main):034:0> p.modify = 1990
=> 1990
irb(main):035:0> p.age
=> 27

# self ambiguity
class WhereDoesItComeFrom
    def self.from
        puts "class method"
    end
    def from
        puts "instance method"
    end
    def which
        from # instand method so it will call the *from* instance method
    end
    def self.which
        from # class method so it will call the *from* class method
    end
    def unambiguous
        self.from # call the instance method
        self.class.from # call the class method
    end
    def self.unambiguous
        # self always refer to an instance
        # but `WhereDoesItComeFrom` is an instance of the class `Class`
        self.from # output: class method
        self.class.from # raise an error because `Class.from` doesn't exist
    end
end

WhereDoesItComeFrom.from # output: class method
WhereDoesItComeFrom.new.from # output: instance method
WhereDoesItComeFrom.which # output: class method
WhereDoesItComeFrom.new.which # output: instance method
WhereDoesItComeFrom.unambiguous # output: 1st line: class method, 2nd line: NoMethodError: undefined method `from' for Class:Class
WhereDoesItComeFrom.new.unambiguous # output: 1st line: instance method, 2nd line: class method
```

## Class methods and variables

```ruby
# Class methods
class Car
    def self.info
        puts "A car"
    end
end
# When there is no needs to instantiate an object.
irb(main):041:0> Car.info
A car
=> nil

# Class variables
class Animals
    @@number = 0
    def initialize
        @@number += 1
    end

    def self.number
        @@number
    end
end

irb(main):066:0> dog = Animals.new
=> #<Animals:0x000055ab32f47260>
irb(main):067:0> cat = Animals.new
=> #<Animals:0x000055ab32f3a9e8>
irb(main):068:0> snake = Animals.new
=> #<Animals:0x000055ab32c0b358>
irb(main):069:0> Animals.number
=> 3

# Class constants
class Universe
    ANSWER = 42
end

irb(main):073:0> Universe::ANSWER
=> 42
```

## `to_s` method

```ruby
def Aclass
end

puts Aclass # to_s is automatically called
puts Aclass.to_s
```

## Inheritance

```ruby
class Language # superclass / base class
    def initialize(style)
        @style = style
    end
    def example
        puts 'printf ("%s", "Hello world!")'
    end
end

class Ruby < Language # subclass / derived class
    def example
        puts 'puts "Hello world!"'
    end
end

r = Ruby.new("OOB")
r.example
# output : puts "Hello world!"
```

## `super` method

```ruby
class Human
    def speak
        print "I am a human"
    end
end

class Avatar < Human
    def speak
        super
        puts " and I'm blue"
    end
end

a = Avatar.new
a.speak
# output : I am a human and I'm blue

class Vehicle
    def initialize(name)
        @name = name
    end
end

class Car < Vehicle
    def initialize(name,wheels)
        super(name)
        @wheels = wheels
    end
    def to_s
        "My #{@name} has #{@wheels} wheels."
    end
end

c = Car.new("Twingo", 4)
puts c
# output : My Twingo has 4.
```

## Operator overloading

```ruby
class Coordinates
    attr_accessor :x, :y
    def initialize(x, y)
        self.x = x
        self.y = y
    end
    def +(obj)
        Coordinates.new(self.x + obj.x, self.y + obj.y)
    end
end

a = Coordinates.new(8,5)
b = Coordinates.new(-1,4)
c = a + b
puts c.x # output: 7
puts c.y # output: 9
```

## Access modifiers

```ruby
# private methods
class My_time
    def initialize(hours)
        @hours = hours
    end
    def convert
        # can call private minutes method because we are in the class
        # but not self.minutes because minutes is not public
        puts "#{@hours} hours(s) = #{minutes} minutes = #{seconds} seconds"
    end
    private
    def minutes
        @hours * 60
    end
    def seconds
        @hours * 3600
    end
end

t = My_time.new(24)
t.convert # output: 24 hours(s) = 1440 minutes = 86400 seconds
puts t.minutes # output: NoMethodError: private method `minutes' called for #<My_time:0x000055b311f9df20 @hours=24>

# protected methods
class Thing
    def initialize
        @id = rand(9999)
    end
    def ==(obj)
        # can call protected (private-like) method from the code but are not public
        self.id == obj.id
    end
    protected
    def id
        @id
    end
end

t1 = Thing.new
t2 = Thing.new
puts (t1 == t2)
# output: false
```

## Abstract classes and methods

Ruby doesn't have a built-in notion of an abstract method or class.

**Note**: it's advised not to use abstract class without proper error management.

An abstract class is a class that should never be instantiated directly.

```ruby
# Abstract class and method
class A
    # abstract class
    def initialize
        raise NotImplementedError
    end
    # abstract method
    def something
        # accessible only for classes which define number method
        puts "number: #{number}"
    end
end

class B < A
    def initialize
        # just need to override parent class
    end
    def number
        4
    end
end

b = B.new
puts b.number # output: 4
puts b.something # output: number: 4
# A.new will raise an error
# if `A` didn't have `raise` in `initialize` method we should have been able to call the
# `something` method but it will have raise an error as the `number` method doesn't exist
# for the `A` class.
```
