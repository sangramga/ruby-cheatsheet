Authors: Alexandre ZANNI

# Basic concepts

## Hello World

```ruby
puts "Hello World!"
```

## Comments

```ruby
# single line comment

=begin
  Multi-lines
  comment
=end
```

## Variables

```ruby
my_var = 42
My_constant_var = 31337
```

## Shorthand assignment

```ruby
a += b # a = a + b
a -= b # a = a - b
a *= b # a = a * b
a /= b # a = a / b
a %= b # a = a % b
a **= b # a = a**b
```

## Parallel assignment

```ruby
a, b, c = 11, 22, 33
```

## Input

```ruby
input = gets
```
