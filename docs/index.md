---
icon: home
title: Welcome
---
Authors: Alexandre ZANNI

# Ruby Cheatsheet

Hi there. It seems you find a very nice Ruby cheat sheet here!

![](https://upload.wikimedia.org/wikipedia/commons/7/73/Ruby_logo.svg)

```ruby
puts "Here we #{"<3" * 3} Ruby!"
```

But don't forget this is a cheat sheet not a training or a MOOC.

This cheat sheet was inspired by [SoloLearn](https://www.sololearn.com/) Ruby MOOC.
